# Site de mon compte

Ce n'est pas un vrai projet, c'est le site de mon compte qui est destiné à regrouper tous les liens vers les documents produits par mes projets.

## Raison

Gitlab fourni automatiquement un site par projet.
Comme j'ai plusieurs projets, j'ai plusieurs sites.
Le but est donc d'avoir un site unique pour distribuer tous mes documents.

## Lecture du document compilé

Le site déployé est accessible ici : [mon site](https://scarpet42.gitlab.io/)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
